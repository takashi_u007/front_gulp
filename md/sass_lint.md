# sass lint（sassを使用する際のルール）

[sass lint rules](https://github.com/sasstools/sass-lint/tree/master/docs/rules)

## ◆カラーコードの指定
・16進数で指定  
・6桁で指定（3桁でも指定できるが、警告を出している）  
・16進数のアルファベットは小文字で記述  

## ◆擬似要素
・ネストで記述（&:before <= この記述）  
・contentはシングルクォーテーションを使用

## ◆ネストに関して（いれごにして記述する場合）
ネストする場合は改行を入れる。  
（ネストは3階層までできるが4階層目から警告）
```
ex) okパターン
.box {
	padding: 10px;

	&.inner {
		margin-bottom: 10px;
	}
}
```
```
ex) 警告パターン
.box {
	padding: 10px;
	&.inner {
		margin-bottom: 10px;
	}
}
```

## ◆セレクタを連続して書く場合はネストして記述
```
ex) okパターン
.box {
	font-size: 12px;

	.text {
		margin: 10px;
	}
}
```
```
ex) 警告パターン
.box {
	font-size: 12px;
}
.box .text {
	margin: 10px;
}
```

## ◆セレクタの後の中カッコに関して
セレクタの後に半角スペースの後に中カッコを書く
```
ex) okパターン
.box {
	padding: 10px;
}
```
```
ex) 警告パターン
.box{
	padding: 10px;
}
```

## ◆セレクタのidは使用可能（cssでidを使用しないのは一般的ですが検討）
ほとんどのcssデバッグツールは警告が出るようになっているが、現状は警告が出ないように設定

## ◆プロパティの後は半角スペースを入れる
```
ex) okパターン
.box {
	margin: 10px;
}
```
```
ex) 警告パターン
.box {
	margin:10px;
}
```

## ◆プロパティの値が1未満の場合、小数点の前の0は抜く
```
ex) okパターン
.text {
	font-size: .5em;
}
```
```
ex) 警告パターン
.text {
	font-size: 0.5em;
}
```
