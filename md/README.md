# frontend gulp

## setup
※ node version 6.0.0以上

**1. git clone**
```
git clone [URL]
```

**2. node modules install**
```
npm install
```

## gulp command

**◆gulp default task**  
・html debug  
・sass compile  
・browserSync  
```
gulp
```

**◆html hint**
```
gulp html
```

**◆sass compile**
```
gulp sass
```

**◆javascript compile**
```
gulp script
```
