var gulp = require('gulp');

var browserSync = require('browser-sync');

var htmlhint = require('gulp-htmlhint');

var sass = require('gulp-sass');
var notify = require('gulp-notify');
var plumber = require('gulp-plumber');
var sassLint = require('gulp-sass-lint');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var cssMqpacker = require('css-mqpacker');
var cleanCss = require('gulp-clean-css');

var eslint = require('gulp-eslint');
var gulpWebpack = require('gulp-webpack');
var webpackConfig = require('./webpack.config.js');

var imagemin = require('gulp-imagemin');

//gulp default command
gulp.task('default', function() {
  browserSync.init({
    server: {
      baseDir: 'dist/'
    }
  });
  gulp.watch('src/**/*.html', ['html']);
  gulp.watch('src/assets/sass/*.+(scss|css)', ['sass']);
});

//html check
gulp.task('html', function() {
  return gulp.src('src/**/*.html')
    .pipe(htmlhint())
    .pipe(htmlhint.reporter())
    .pipe(gulp.dest('dist/'))
    .pipe(browserSync.stream());
});

//sass build
var browsers = [
  '> 3%',
  'Last 3 versions'
];
gulp.task('sass', function() {
  return gulp.src('src/assets/sass/*.+(scss|css)')
    .pipe(plumber({
      errorHandler: notify.onError('Error: <%= error.message %>')
    }))
    .pipe(sassLint())
    .pipe(sassLint.format())
    .pipe(sassLint.failOnError())
    .pipe(sass())
    .pipe(postcss([
      autoprefixer({
        browsers: browsers
      }),
      cssMqpacker()
    ]))
    .pipe(gulp.dest('dist/assets/css/'))
    .pipe(notify({
      title:   'complete sass build',
      message: new Date()
    }))
    .pipe(browserSync.stream());
});

//css minify
gulp.task('cssmin', function(){
  gulp.src('dist/assets/css/*.css')
  .pipe(cleanCss({
    keepBreaks: false
  }))
  .pipe(gulp.dest('dist/assets/css/'))
  .pipe(notify({
    title:   'complete css minify',
    message: new Date()
  }))
  .pipe(browserSync.stream());
});

//script build
gulp.task('script', function() {
  return gulp.src('src/assets/js/*.js')
    .pipe(plumber({
      errorHandler: notify.onError('Error: <%= error.message %>')
    }))
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError())
    .pipe(gulpWebpack(webpackConfig))
    .pipe(gulp.dest('dist/assets/js/'));
});

//image

//image minify
gulp.task('imagemin', function(){
  gulp.src('dist/assets/image/*.+(jpg|png|svg)')
  .pipe(imagemin())
  .pipe(gulp.dest('dist/assets/image/'))
  .pipe(browserSync.stream());
});
