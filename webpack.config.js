'use strict';

var webpack = require('webpack');

module.exports = {
  entry:  __dirname + '/src/assets/js/app.js',
  output: {
    path:     __dirname + '/dist/assets/js/',
    filename: 'bundle.js'
  },
  resolve: {
    extensions: ['', '.webpack.js', '.web.js', '.js']
  },
  module: {
    loaders: [
      {
        test:    /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader:  'babel',
        query:   {
          presets: ['es2015']
        }
      }
    ]
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin()
  ],
  devtool: 'source-map'
};
